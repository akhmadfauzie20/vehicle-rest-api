<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vehicle;
use App\Models\Car;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicles = Vehicle::all();

        foreach($vehicles as $vehicle) {
            $cars = Car::factory()->count(rand(1, 3))->create([
                'vehicles_id' => $vehicle->id
            ]);
        }
    }
}
