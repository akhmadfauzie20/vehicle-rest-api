<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vehicle;
use App\Models\Motorcycle;

class MotorcycleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicles = Vehicle::all();

        foreach($vehicles as $vehicle) {
            $motor = Motorcycle::factory()->count(rand(1, 3))->create([
                'vehicles_id' => $vehicle->id
            ]);
        }
    }
}
