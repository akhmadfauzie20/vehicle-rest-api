<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VehicleSeeder::class);
        $this->call(CarSeeder::class);
        $this->call(MotorcycleSeeder::class);
    }
}
