<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transactions_id')->constrained('cascade')->onDelete('cascade')->onUpdate('cascade');
            $table->string('vehicle_year');
            $table->string('colour');

            // Car
            $table->string('machine_type')->nullable();
            $table->integer('capacity')->nullable();
            $table->string('type')->nullable();

            // Motorycle
            $table->string('machine_type')->nullable();
            $table->string('suspension_type')->nullable();
            $table->string('transmission_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
