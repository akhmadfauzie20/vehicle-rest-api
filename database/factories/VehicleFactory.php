<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'vehicle_year' => $this->faker->year(),
            'colour' => $this->faker->safeColorName(),
            'price' => $this->faker->randomNumber(7, true)
        ];
    }
}
