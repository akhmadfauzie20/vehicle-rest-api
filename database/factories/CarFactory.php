<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'machine_type' => $this->faker->postcode(),
            'capacity' => random_int(2, 4),
            'type' => $this->faker->postcode()
        ];
    }
}
