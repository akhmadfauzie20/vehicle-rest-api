<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MotorcycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'machine_type' => $this->faker->postcode(),
            'suspension_type' => random_int(4, 10),
            'transmission_type' => $this->faker->randomElement(['Manual', 'Automatic'])
        ];
    }
}
