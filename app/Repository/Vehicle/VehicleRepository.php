<?php

namespace App\Repository\Vehicle;

class VehicleRepository {

    protected $model = null;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function car()
    {
        return $this->model->car()->get();
    }

    public function motorcycle()
    {
        return $this->model->motorcycle()->get();
    }
}

?>
