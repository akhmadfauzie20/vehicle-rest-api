<?php

namespace App\Repository\Transaction;
use Auth;

class TransactionRepository {

    protected $model = null;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function cart()
    {
        return $this->model
                    ->where('user_id', Auth::id())
                    ->where('status', 'pending')
                    ->get();
    }

    public function addToCart($data)
    {
        return $this->model->create($data);
    }

    public function checkout($trx_id, $trx)
    {
        return $this->model->where($trx_id)->update($trx);
    }

    public function transaction()
    {
        return $this->model->where('status', 'done')->get();
    }

    public function detail($id)
    {
        return $this->model->where('_id', $id)->where('status', 'done')->first();
    }
}

?>
