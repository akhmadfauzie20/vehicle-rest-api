<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\Vehicle;

class Car extends Eloquent implements JWTSubject
{
    use HasFactory;

    public function vehicle() {
        return $this->belongsToMany(Vehicle::class);
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
