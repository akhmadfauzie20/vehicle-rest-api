<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Vehicle;

class Transaction extends Eloquent implements JWTSubject
{
    protected $fillable = [
        'vehicles_id',
        'user_id',
        'vehicle_type',
        'price',
        'status',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function vehicle() {
        return $this->hasOne(Vehicle::class, 'vehicles_id');
    }

    public function detail() {
        return $this->belongsToMany(TransactionDetail::class);
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
