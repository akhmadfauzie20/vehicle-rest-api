<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\Vehicle;

class Motorcycle extends Eloquent implements JWTSubject
{
    use HasFactory;

    public function vehicle() {
        return $this->hasOne(Vehicle::class, 'vehicles_id');
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
