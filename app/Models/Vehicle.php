<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\Car;
use App\Models\Motorcycle;
use App\Models\Transactions;

class Vehicle extends Eloquent implements JWTSubject
{
    use HasFactory;

    public function car() {
        return $this->belongsToMany(Car::class);
    }

    public function motorcycle() {
        return $this->belongsToMany(Motorcycle::class);
    }

    public function transaction() {
        return $this->belongsToMany(Transaction::class);
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
