<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Vehicle\VehicleRepository;

class VehicleController extends Controller
{
    public function cars(VehicleRepository $repository)
    {
        $cars = $repository->car();

        return response()->json([
            'success' => true,
            'vehicles' => $cars
        ]);
    }

    public function motorcycles(VehicleRepository $repository)
    {
        $motorcycles = $repository->motorcycle();

        return response()->json([
            'success' => true,
            'vehicles' => $motorcycles
        ]);
    }
}
