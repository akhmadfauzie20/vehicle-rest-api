<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use App\Models\Car;
use App\Models\Vehicle;
use App\Models\Motorcycle;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\TransactionDetail;
use App\Http\Controllers\Controller;
use App\Repository\Transaction\TransactionRepository;

use Auth;

class TransactionController extends Controller
{
    public function index(TransactionRepository $repository) {
        $trx = $repository->transaction();

        return response()->json([
            'success' => true,
            'transaction' => $trx
        ]);
    }

    public function detail(TransactionRepository $repository, $id) {
        $trx = $repository->detail($id);

        return response()->json([
            'success' => true,
            'transaction' => $trx
        ]);
    }

    public function cart(TransactionRepository $repository) {
        $cart = $repository->cart();

        return response()->json([
            'success' => true,
            'item' => $cart
        ]);
    }

    public function addToCart(TransactionRepository $repository, Request $request) {
        $validator = Validator::make($request->all(),
        [
            'vehicle_type' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $trx = Transaction::query()
                          ->where('user_id', Auth::id())
                          ->where('status', 'pending')
                          ->first();


        if (empty($trx)) {
            $vehicle_type = $request->vehicle_type;

            if($vehicle_type == 'Car') {
                $vehicle = Car::query()
                              ->where('machine_type', $request->type)
                              ->first();
            } elseif ($vehicle_type == 'Motorcycle') {
                $vehicle = Motorcycle::query()
                                     ->where('machine_type', $request->type)
                                     ->first();
            }

            if(!empty($vehicle)) {
                $v = Vehicle::query()->where('_id', $vehicle->vehicles_id)->first();

                $data = [
                    'vehicles_id' => $vehicle->id,
                    'user_id' => Auth::id(),
                    'vehicle_type' => $vehicle_type,
                    'price' => $v->price,
                    'status' => 'pending'
                ];

                $transaction = $repository->addToCart($data);

                $detail = new TransactionDetail();
                $detail->transaction_id = $transaction->id;
                $detail->vehicle_year = $v->vehicle_year;
                $detail->colour = $v->colour;

                if ($vehicle_type == 'Car') {
                    $detail->machine_type = $vehicle->machine_type;
                    $detail->capacity = $vehicle->capacity;
                    $detail->type = $vehicle->type;
                } else {
                    $detail->machine_type = $vehicle->machine_type;
                    $detail->suspension_type = $vehicle->suspension_type;
                    $detail->transmission_type = $vehicle->transmission_type;
                }

                $detail->save();

                return response([
                    'success' => true,
                    'item' => $detail,
                    'msg' => 'Success adding item to your cart.'
                ]);
            }
        } else {
            return response([
                'success' => false,
                'item' => [],
                'msg' => 'Sorry, you already have pending item in your cart.'
            ]);
        }
    }

    public function checkout(TransactionRepository $repository, $id)
    {
        // $validator = Validator::make($request->all(),
        // [
        //     'transaction_id' => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return response()->json(['error' => $validator->errors()], 401);
        // }

        $transaction = Transaction::query()
                                  ->where('_id', $id)
                                  ->where('status', 'pending')
                                  ->first();

        if (!empty($transaction)) {
            $data = [
                'status' => 'done'
            ];

            $trx = $repository->checkout($id, $data);

            return response([
                'success' => true,
                'trx' => $trx,
                'msg' => 'Success checkout item from your cart.'
            ]);
        } else {
            return response([
                'success' => false,
                'trx' => [],
                'msg' => 'transaction is not available.'
            ]);
        }
    }
}
