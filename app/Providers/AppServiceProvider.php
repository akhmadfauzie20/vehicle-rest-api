<?php

namespace App\Providers;

use App\Models\Vehicle;
use App\Models\Transaction;
use Illuminate\Support\ServiceProvider;
use App\Repository\Vehicle\VehicleRepository;
use App\Repository\Transaction\TransactionRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VehicleRepository::class, function ($app) {
            return new VehicleRepository($app->make(Vehicle::class));
        });

        $this->app->bind(TransactionRepository::class, function ($app) {
            return new TransactionRepository($app->make(Transaction::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
