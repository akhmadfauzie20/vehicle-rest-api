<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard\VehicleController;
use App\Http\Controllers\Dashboard\TransactionController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout']);

Route::group(['middleware' => 'jwt.auth'], function () {

    Route::prefix('vehicle')->group(function () {
        Route::get('/car', [VehicleController::class, 'cars']);
        Route::get('/motorcycle', [VehicleController::class, 'motorcycles']);
    });

    Route::prefix('cart')->group(function () {
        Route::get('/', [TransactionController::class, 'cart']);
        Route::post('/buy', [TransactionController::class, 'addToCart']);
        Route::put('/checkout/{id}', [TransactionController::class, 'checkout']);
    });

    Route::prefix('transaction')->group(function () {
        Route::get('/', [TransactionController::class, 'index']);
        Route::get('/detail/{id}', [TransactionController::class, 'detail']);
    });
});
