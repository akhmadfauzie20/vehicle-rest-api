<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Transaction;
use Tests\TestCase;

use Auth;
class BasicTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHasItemInCart()
    {
        $trx = new Transaction([
            'vehicles_id' => '0',
            'user_id' => Auth::id(),
            'vehicle_type' => 'Car',
            'price' => '10000',
            'status' => 'pending'
        ]);

        $this->assertEquals('10000', $trx->price);
    }
}
