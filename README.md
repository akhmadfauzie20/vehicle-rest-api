# Vehicle-REST-API

Vehicle REST-API using Laravel

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Installation
- Installing PHP dependencies by running `composer install`
- Copy .env.example to .env and fills required info
- Copy `database/api-testing` to Insomnia to see the list api
- Data setup by running `php artisan migrate:fresh --seed`
- Last but not least, run `php artisan serve` to be the api host.
